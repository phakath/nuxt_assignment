export const state = () => ({
  order: [],
  totalprice: 0,
  discount: 0,
  code: null,
})
export const mutations = {
  addOrder(state, { items }) {
    const index = state.order.findIndex((el) => el.productID == items.productID)

    if (index != -1) {
      state.order[index].productAmount = items.productAmount
      state.order = [...state.order]
    } else state.order = [...state.order, items]
  },
  toggleCart() {
    var element = document.getElementsByTagName('body')
    if (element[0].classList.contains('open'))
      return element[0].classList.remove('open')
    element[0].classList.add('open')
  },
  deleteOrderItems(state, { index }) {
    console.log(index)
    state.order.splice(index, 1)
  },
}
// export const getters = {
//   // ...
//   doneTodosCount(state, getters) {
//     console.log(state)
//     console.log(getters);
//     return getters.doneTodos.length
//   },
// }
