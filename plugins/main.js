import Vue from 'vue'

import Vuelidate from 'vuelidate'
import VueMoment from 'vue-moment'
const moment = require('moment')
moment.tz.setDefault('Asia/Bangkok')
Vue.use(VueMoment, { moment })
Vue.use(Vuelidate)
